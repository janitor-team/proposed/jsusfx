#!/bin/sh
OUTDIR=liteon
if [ -d "${OUTDIR}" ]; then
 exit 0
fi
cp -r scripts/liteon "${OUTDIR}"
find "${OUTDIR}/" -type f -not -name "LICENSE*" -not -name "README*" -exec mv {} {}.jsfx \;
